set terminal pngcairo size 2700,1500 enhanced font 'arial, 30' lw 3
set output sprintf("sidelobe_free_filter.png")

set multiplot layout 2,2\
    margins 0.05,0.98,0.1,0.88\
    spacing 0.05,0.18

set zeroaxis
set xtics 5
set grid

file = 'filter.dat'
set title "Hc"
plot file index 0 using 1:2 with linespoints title 'Real' lw 2

set title "FFT from Hc"
plot file index 1 using 1:2 with linespoints title 'Real' lw 2,\
    file index 1 using 1:3 with linespoints title 'Image' lw 2


set title "convolution from Hc and p"
set yrange [-1.1:1.1]
plot file index 2 using 1:2 with linespoints title 'Real' lw 2
unset yrange

set title "FFT from convolution"
plot file index 3 using 1:2 with linespoints title 'Real' lw 2,\
    file index 3 using 1:3 with linespoints title 'Image' lw 2
