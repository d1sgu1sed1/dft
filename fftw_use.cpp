#include "fftw_use.h"

void using_fftw(const int N, fftw_complex* in, fftw_complex* out, bool is_forward){
    fftw_plan p;
    if(is_forward){
        p = fftw_plan_dft_1d(N, in, out, FFTW_FORWARD, FFTW_ESTIMATE);
        fftw_execute(p);
    }
    else{
        p = fftw_plan_dft_1d(N, in, out, FFTW_BACKWARD, FFTW_ESTIMATE);
        fftw_execute(p);
    }
    fftw_destroy_plan(p);
}


void print_to_file(const char *filename, const int N, fftw_complex* fftw_stream, bool is_mirror){
    ofstream file_out(filename);
    const int k = N % 2 == 0 ? N / 2 : N / 2 - 1;
    if(is_mirror){
        for (int i = 0; i < k; i++)
            file_out << i - k << " " << fftw_stream[i + k][0] << " " << fftw_stream[i + k][1] << "\n";
        for (int i = 0; i < k; i++) 
            file_out << i << " " << fftw_stream[i][0] << " " << fftw_stream[i][1] << "\n";
    }
    else
        for (int i = 0; i < N; i++) 
            file_out << i << " " << fftw_stream[i][0] << " " << fftw_stream[i][1] << "\n";
        
    file_out.close();
}

void print_to_file_vec(const char *filename, vector<complex<double> > vec, bool is_mirror){
    ofstream file_out(filename);
    for (int i = 0; i < vec.size(); i++)
        file_out << i << " " << real(vec[i]) << " "<< imag(vec[i]) << "\n";
    
    file_out.close();
}

void print_to_file_vec_arr(const char *filename, vector<vector<complex<double> > > vec){
    ofstream file_out(filename);
    for(int j = 0; j < vec.size(); j++){
        for (int i = 0; i < vec[j].size(); i++) 
            file_out << i << " " << real(vec[j][i]) << " " << imag(vec[j][i]) << "\n";
        file_out << "\n\n";
    }
    
        
    file_out.close();
}
