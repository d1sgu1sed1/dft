#include <iostream>
#include "math_funcs.h"
#include "fftw_use.h"
#include "constants.h"

using namespace std;

int main(){
    fftw_complex *in, *out, *conv_dft, *Hc_dft;
    conv_dft = (fftw_complex *)fftw_malloc(sizeof(fftw_complex) * N);
    Hc_dft = (fftw_complex *)fftw_malloc(sizeof(fftw_complex) * N);
    in = (fftw_complex *)fftw_malloc(sizeof(fftw_complex) * N);
    out = (fftw_complex *)fftw_malloc(sizeof(fftw_complex) * N);

    vector<complex<double> > Hc(N);
    vector<complex<double> > Hc_dft_vec(N);
    vector<complex<double> > conv_vec(N);
    vector<complex<double> > t(N);
    vector<complex<double> > conv_dft_vec(N);
    vector<complex<double> > hc_multi_conv(N);
    vector<complex<double> > filtered_data(N);
    vector<vector<complex<double> > > vec2print;

    barker_code(bits, in);

    for(int i = 0; i < N; i++)
        Hc[i] = i < N ? complex<double> (h(in, bits, i, units), 0.0) : complex<double> (0.0, 0.0);
    
    vec2print.push_back(Hc);
    vector2fftwcmpl(N, Hc, in);
    using_fftw(N, in, Hc_dft, true);
    for(int i = 0; i < N; i++){
        complex<double> x(Hc_dft[i][0], Hc_dft[i][1]);
        Hc_dft[i][0] = real(1.0 / x);
        Hc_dft[i][1] = imag(1.0 / x);
    }
    fftwcmpl2vector(N, Hc_dft_vec, Hc_dft);
    vec2print.push_back(Hc_dft_vec);
    
    for(int i = 0; i < N; i++)
        in[i][0] = i < N ? convolution(i, units, bits, true) : 0;

    fftwcmpl2vector(N, conv_vec, in);
    vec2print.push_back(conv_vec);

    using_fftw(N, in, conv_dft, true);

    for (int i = 0; i < k; i++)
        conv_dft_vec[i] = complex<double>(conv_dft[i + k][0], conv_dft[i + k][1]);
    for (int i = 0; i < k; i++)
        conv_dft_vec[i + k] = complex<double>(conv_dft[i][0], conv_dft[i][1]);
    vec2print.push_back(conv_dft_vec);

    for(int i = 0; i < N; i++){
        in[i][0] = conv_dft[i][0] * Hc_dft[i][0] - conv_dft[i][1] * Hc_dft[i][1];
        in[i][1] = conv_dft[i][1] * Hc_dft[i][0] + conv_dft[i][0] * Hc_dft[i][1];
    }

    fftwcmpl2vector(N, conv_dft_vec, in);
    vec2print.push_back(conv_dft_vec);

    using_fftw(N, in, out, false);
    fftwcmpl2vector(N, filtered_data, out);
    vec2print.push_back(filtered_data);

    print_to_file_vec_arr("filter.dat", vec2print);

    fftw_free(in);
    fftw_free(out);
    fftw_free(conv_dft);
    fftw_free(Hc_dft);

}