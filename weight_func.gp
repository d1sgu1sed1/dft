set terminal pngcairo size 2700,1500 enhanced font 'arial, 30' lw 2
set output sprintf("weight_func.png")

set multiplot layout 3,1\
    margins 0.08,0.95,0.09,0.93\
    spacing 0.1,0.12   

set zeroaxis
set xtics 5
set grid

file = 'weight_func.dat'

set xrange[0:15]
set yrange[-1.1:1.1]
set title "convolution Hc and p"
plot file using 1:2 index 0 with linespoints title 'Real' lw 2
set title "reversed convolution Hc and p"
plot file using 1:2 index 1 with linespoints title 'Real' lw 2 
unset yrange
set xrange[0:30]
set title "weight function from convolutions"
plot file using 1:2 index 2 with linespoints title 'Real' lw 2





