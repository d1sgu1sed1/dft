#include <fftw3.h>
#include <iostream>
#include <cmath>
#include <complex>
#include <vector>

#ifndef _MATH_FUNCS_H
#define _MATH_FUNCS_H

using namespace std;

// Математические функции
void delta_func(const int N, fftw_complex* in);
void rect_func(const int N, fftw_complex* in);
void rect_func_reversed(const int N, fftw_complex *in);
void cos_func(const int N, fftw_complex* in);
void gauss_func(const int N, fftw_complex* in);

int impulse_func(const int N);
int p_func(const int n, const int units);
vector<double> q_func(vector<double> p);
int h(fftw_complex* in, const int bits, const int n, const int units);
int convolution(const int n, const int units, const int bits, bool is_forward);
void reverse_fftwcmpl(const int N, fftw_complex* in);
void reverse_vector(const int N, vector<double> &in);
void vector2fftwcmpl(const int N, vector<complex<double> > &H, fftw_complex* in);
void fftwcmpl2vector(const int N, vector<complex<double> > &H, fftw_complex* in);

void barker_code(const int N, fftw_complex* in);


#endif
