set terminal pngcairo size 2700,1500 enhanced font 'arial, 30' lw 3
set output sprintf("filtered_data.png")

set multiplot layout 2,1\
    margins 0.05,0.98,0.1,0.88\
    spacing 0.05,0.18

set zeroaxis
set xtics 5
set grid

file = 'filter.dat'

set title "multiplication hc and convolution spectrums"
plot file index 4 using 1:2 with linespoints title 'Real' lw 2,\
    file index 4 using 1:3 with linespoints title 'Image' lw 2
set title "backward FFT from multiplication"
set yrange [-1:31]
plot file index 5 using 1:2 with linespoints title 'Real' lw 2





