#
CXX = g++
CXXFLAGS = -Wextra -g3 -O3 -std=c++14 -fopenmp -lpthread -I/gnuplot-iostream
#
main: constants.h main.o math_funcs.o fftw_use.o 
	$(CXX) $(CXXFLAGS) -o main constants.h main.o math_funcs.o fftw_use.o -lfftw3
weight: weight_func.o math_funcs.o fftw_use.o
	$(CXX) $(CXXFLAGS) -o weight_func weight_func.o math_funcs.o fftw_use.o -lfftw3
sidelobe_filter: sidelobe_filter.o math_funcs.o fftw_use.o
	$(CXX) $(CXXFLAGS) -o sidelobe_filter sidelobe_filter.o math_funcs.o fftw_use.o -lfftw3
clean:
	rm *.o
	rm *.dat
