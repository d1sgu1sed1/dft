#include "fftw_use.h"
#include "math_funcs.h"
#include "constants.h"

using namespace std;

int main(){
    fftw_complex *in;
    in = (fftw_complex *)fftw_malloc(sizeof(fftw_complex) * N);

    vector<complex<double> > p(N);
    vector<complex<double> > Hc(N);
    vector<complex<double> > conv(N);
    vector<complex<double> > conv2(N);
    vector<complex<double> > conv3(2 * N + 1);
    vector<vector<complex<double> > > vec2print;

    for(int i = 0; i < N; i++)
        p[i] = p_func(i, units);

    barker_code(N, in);
    for(int i = 0; i < N; i++)
        Hc[i] = h(in, bits, i, units);

    for(int i = 0; i < N; i++)
        conv[i] = convolution(i, units, bits, true);
    vec2print.push_back(conv);

    for(int i = 0; i < N; i++)
        conv2[i] = convolution(i, units, bits, false);
    vec2print.push_back(conv2);

    for(int i = 0; i < 2 * N; i++){
        conv3[i] = 0;
        for(int j = 0; j < N; j++)
            if(i - j >= 0 && i - j <= N)
                conv3[i] += conv[j] * conv2[i - j - 1];
    }
    
    vec2print.push_back(conv3);
    print_to_file_vec_arr("weight_func.dat", vec2print);
}