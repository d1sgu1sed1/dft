#include <complex>
#ifndef _CONSTANTS_H
#define _CONSTANTS_H

using namespace std;

const complex<double> im{0.0, -1.0};
const int bits = 13; // Скольки битный код баркера(5, 7, 11, 13)
const int units = 3; // Кол-во отсчётов за бит
const int N = 2 * bits * units; // Длина графика
const int k = N % 2 == 0 ? N / 2 : N / 2 - 1;
const double frequency = 100.0;// Частота косинуса
const double sigma = 50.0; // Коэффициент распределения

#endif