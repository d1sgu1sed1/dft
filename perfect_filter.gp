set terminal pngcairo size 2700,1500 enhanced font 'arial, 30' lw 3
set output sprintf("perfect_filter.png")

set multiplot layout 1,1\
    margins 0.05,0.98,0.1,0.88\
    spacing 0.05,0.18

set zeroaxis
set xtics 5
set grid

file = 'PowerACF.dat'

set title "perfect filter"
plot file using 1:2 with linespoints title 'Real' lw 2

