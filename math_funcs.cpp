#include "math_funcs.h"
#include "constants.h"

void delta_func(const int N, fftw_complex* in){
    for (int i = 0; i < N; i++) {
        in[i][0] = (i == 0) ? 1.0 : 0.0; 
        in[i][1] = 0.0;
    }
}

void rect_func(const int N, fftw_complex *in){
    for (int i = 0; i < N; i++) {
        in[i][0] = (i < units) ? 1.0 : 0.0; 
        in[i][1] = 0.0;
    }
}

void cos_func(const int N, fftw_complex *in){
    for (int i = 0; i < N; ++i) {
        in[i][0] = cos(2 * M_PI * frequency * i / N);
        in[i][1] = 0.0;
    }
}

void gauss_func(const int N, fftw_complex *in){
    for (int i = 0; i < N; ++i) {
        double x = i - N / 2.0;
        in[i][0] = exp(-0.5 * (x / sigma) * (x / sigma));
        in[i][1] = 0.0;
    }
}

int p_func(const int n, const int units){
    int sum = 0;
    for(int i = 0; i < units; i++)
        sum += impulse_func(i - n);
    
    return sum;
}

vector<double> q_func(vector<double> p){
    vector<double> result;
    for (int i = 0; i < p.size(); ++i)
        result.push_back(p[p.size() - 1 - i]);

    return result;
}

int h(fftw_complex *in, const int bits, const int n, const int units){
    int sum = 0;
    for (int i = 0; i < bits; i++) 
        sum += in[i][0] * impulse_func(n - i * units);
    
    return sum;
}

void vector2fftwcmpl(const int N, vector<complex<double> > &H, fftw_complex* in){
    for(int i = 0; i < N; i++){
        in[i][0] = real(H[i]);
        in[i][1] = imag(H[i]);
    }
}

void fftwcmpl2vector(const int N, vector<complex<double> > &H, fftw_complex* in){
    for(int i = 0; i < N; i++)
        H[i] = complex<double> (in[i][0], in[i][1]);
}

void reverse_fftwcmpl(const int N, fftw_complex* in){
    fftw_complex* in_temp;
    in_temp = (fftw_complex *)fftw_malloc(sizeof(fftw_complex) * N);
    for(int i = 0; i < N; i++)
        in_temp[i][0] = in[i][0];

    for(int i = 0, j = N - 1; i < N; i++, j--){
        in[i][0] = in_temp[j][0];
        in[i][1] = 0;
    }
}

void reverse_vector(const int N, vector<double> &in){
    vector<double> in_temp;
    in_temp = in;

    for(int i = 0, j = N - 1; i < N; i++, j--)
        in[i] = in_temp[j];
    
}

int convolution(const int n, const int units, const int bits, bool is_forward){
    int sum = 0; 
    fftw_complex *in;
    in = (fftw_complex *)fftw_malloc(sizeof(fftw_complex) * bits);
    barker_code(bits, in);
    if(is_forward){
        for(int i = 0; i < bits; i++)
            sum += p_func(i, units) * h(in, bits, n - i, units);
    }
    else{
        reverse_fftwcmpl(bits, in);
        for(int i = -units; i < units; i++)
            sum += p_func(i, units) * h(in, bits, n - i, units);
    }
    return sum;
}

int impulse_func(const int N){
    return N == 0 ? 1 : 0;
}

void barker_code(const int N, fftw_complex *in){
    switch(N){
        case 5:
            for (int i = 0; i < N; ++i) {
                in[i][0] = (i == 3) ? -1.0 : 1.0;
                in[i][1] = 0.0;
            }
            break;
        case 7:
            for (int i = 0; i < N; ++i) {
                in[i][0] = (i == 3 || i == 4 || i == 6) ? 0.0 : 1.0;
                in[i][1] = 0.0;
            }
            break;
        case 11:
            for (int i = 0; i < N; ++i) {
                in[i][0] = (i == 0 || i == 1 || i == 2 || i == 6 || i == 9) ? 1.0 : 0.0;
                in[i][1] = 0.0;
            }
            break;
        case 13:
            for (int i = 0; i < N; ++i) {
                in[i][0] = (i == 5 || i == 6 || i == 9 || i == 11) ? -1.0 : 1.0;
                in[i][1] = 0.0;
            }
            break;
    }
}