#include <iostream>
#include "math_funcs.h"
#include "fftw_use.h"
#include "constants.h"

using namespace std;

int main(){
    const int K = 100;
    const int P = 39;
    vector<complex<double> > clear_acf(K);
    vector<complex<double> > hc_and_p(K);
    fftw_complex *in, *out, *dft_acf;
    in = (fftw_complex *)fftw_malloc(sizeof(fftw_complex) * K + P - 1);
    dft_acf = (fftw_complex *)fftw_malloc(sizeof(fftw_complex) * K);
    out = (fftw_complex *)fftw_malloc(sizeof(fftw_complex) * K);
    for(int i = 0; i < K; i++){
        switch (i)
        {
        case K / 2:
            clear_acf[i] = 39;
            break;

        case K / 2 - 1:
        case K / 2 + 1:
            clear_acf[i] = 26;
            break;

        case K / 2 - 2:
        case K / 2 + 2:
            clear_acf[i] = 13;
            break;
        
        default:
            clear_acf[i] = 0;
            break;
        }
    }
    
    for(int i = 0; i < K; i++)
        hc_and_p[i] = (i < P) ? convolution(i, units, bits, true) : 0;
    
    vector2fftwcmpl(K, clear_acf, in);
    using_fftw(K, in, dft_acf, true);
    vector2fftwcmpl(K, hc_and_p, in);
    using_fftw(K, in, out, true);
    for(int i = 0; i < K; i++){
        complex<double> t1(in[i][0], in[i][1]), t2(out[i][0], out[i][1]);
        in[i][0] = real(t1 / t2);
        in[i][1] = imag(t1 / t2);
    }
    using_fftw(K + P - 1, in, out, false);
    for(int i = 0; i < K + P - 1; i++){
        out[i][0] = norm(complex<double>(out[i][0], out[i][1]));
        out[i][1] = 0;
    }
    print_to_file("PowerACF.dat", K + P - 1, in, true);

}